
//MATH/NUMBERS
//1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.  
//What is the surface area for each of these pizzas?
    //Small Pizza Surface Area
const pizzaOneDiameter = 13;
const smallPizzaArea = Math.PI * (pizzaOneDiameter / 2) ** 2;
console.log(`The surface are of a small pizza is ${smallPizzaArea.toFixed(2)}`);
    //Large Pizza Surface Area
const pizzaTwoDiameter = 17;
const largePizzaArea = Math.PI * (17 / 2) ** 2;
console.log(`The surface are of a large pizza is ${largePizzaArea.toFixed(2)}`)


//2. What is the cost per square inch of each pizza?
    //Cost per square inch for small pizza
const pizzaOne = 16.99;
const costPerSqInchSmallPizza = pizzaOne / smallPizzaArea;
console.log(`The cost per sq. inch for a small pizza is ${costPerSqInchSmallPizza.toFixed(2)}`);
    //Cost per square inch for large pizza
const pizzaTwo = 19.99;
const costPerSqInchLarePizza = pizzaTwo / largePizzaArea;
console.log(`Cost per square in of a large pizza is ${costPerSqInchLarePizza.toFixed(2)}`)



//3. Using the Math object, put together a code snippet that
//allows you to draw a random card with a value between 1
//and 13 (assume ace is 1, jack is 11…).
//const randomCard = Math.floor(Math.random() * 13) + 1);

const randomNum1 = Math.random() * 13;
const cardValue1 = Math.ceil(randomNum1);

const randomNum2 = Math.random() * 13;
const cardValue2= Math.ceil(randomNum2);

const randomNum3 = Math.random() * 13;
const cardValue3 = Math.ceil(randomNum3);

console.log(cardValue1, cardValue2, cardValue3);

//4. Draw 3 cards and use Math to determine the highest card
const maxValue = Math.max(cardValue1, cardValue2, cardValue3);
console.log(maxValue);



//STRINGS/ADDRESSES
//1. Create variables for firstName, lastName,
//streetAddress, city, state, and zipCode. Use
//this information to create a formatted address block that
//could be printed onto an envelope.
const firstName = 'Zach';
const lastName = 'Cooper';
const streetAddress = '9031 NE 145th Pl';
const city = 'Kenmore';
const state = 'Washington';
const zipCode = '98028';





const combinedAddress = `${firstName} ${lastName}`
${streetAdress}

const newAddress = `Zach Cooper
                9031 NE 145th Pl
                Kenmore
                Washington
                98028`;

console.log(combinedAddress)


//2. You are given a string in this format:
//firstName lastName (assume no spaces in either)
//streetAddress
//city, state zip (could be spaces in city and state)
//– Write code that is able to extract the first name from this string into
//a variable. Hint: use indexOf, slice, and/or substring



//DATES
//On your own find the middle date(and time) between the following two dates:
//1/1/2020 00:00:00 and 4/1/2020 00:00:00